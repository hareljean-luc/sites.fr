﻿#-*-coding: utf-8-*

from copy import deepcopy
from random import *
from time import time
from tkinter import *
from donnees import *

sudo = Tk()
sudo.title('Sudokus')
sudo.geometry('800x800')

COTE=3
TAILLE=COTE*COTE
LONGUEUR=TAILLE*TAILLE

cases_a_traiter=[[k,[i for i in range(1,TAILLE+1)]] for k in range(TAILLE*TAILLE)]
cases_traitees=[]

enregistrement=[]
solution=[]
nbretour=0
nbsol=0
liste_donnees=[]
enonces_faciles=[]
solutions_faciles=[]
enonces_moyens=[]
solutions_moyens=[]
enonces_difficiles=[]
solutions_difficiles=[]
affichage=[]
nombre_faciles = 0
nombre_moyens=0
nombre_difficiles=0

tailles = ['4x4','5x5','6x6']
compteur_affichage=0
liste_entrees=[[]for k in range(TAILLE)]
tableau_solution=[[]for k in range(TAILLE)]
termine=Label(sudo, text ='Terminé')
echec=Label(sudo,text='Sudoku impossible')
succes=Label(sudo)
text_nb_faciles=Label(sudo,text='nombre de solutions faciles désirées')
nb_faciles=Entry(sudo)
text_nb_moyens=Label(sudo,text='nombre de solutions moyennes désirées')
nb_moyens=Entry(sudo)
text_nb_difficiles=Label(sudo,text='nombre de solutions difficiles désirées')
nb_difficiles=Entry(sudo)
espace=Label(sudo, text ='')



def initialisation(liste_donnees):
    global cases_a_traiter
    for case in liste_donnees :
        numero=case[0]
        cases_a_traiter[numero][0]=numero
        cases_a_traiter[numero][1]=[case[1]]

def numero_ligne(case) :
    return case[0]//TAILLE
def numero_colonne(case) :
    return case[0]%TAILLE
def numero_carre(case) :
    abscisse = (case[0]%TAILLE)//COTE
    ordonnee = case[0]//(TAILLE*COTE)
    return COTE*ordonnee+abscisse

def valeurs_manquantes(numero_de_ligne) :
    liste = [k+1 for k in range(TAILLE)]
    for case in cases_traitees :
        if numero_ligne(case) == numero_de_ligne and case[1] in liste:
            liste.remove(case[1])
    return liste

def valeurs_manquantes(numero_de_colonne) :
    liste = [k+1 for k in range(TAILLE)]
    for case in cases_traitees :
        if numero_colonne(case) == numero_de_colonne and case[1] in liste:
            liste.remove(case[1])
    return liste

def valeurs_manquantes(numero_de_carre) :
    liste = [k+1 for k in range(TAILLE)]
    for case in cases_traitees :
        if numero_carre(case) == numero_de_carre and case[1] in liste:
            liste.remove(case[1])
    return liste

def places_possibles_sur_ligne(valeur,numero_de_ligne) :
    liste=[]
    if valeur in valeurs_manquantes(numero_de_ligne) :
        for case in cases_a_traiter :
            if numero_ligne(case)==numero_de_ligne and valeur in case[1] :
                liste.append(case)
    return liste

def places_possibles_sur_colonne(valeur,numero_de_colonne) :
    liste=[]
    if valeur in valeurs_manquantes(numero_de_colonne) :
        for case in cases_a_traiter :
            if numero_colonne(case)==numero_de_colonne and valeur in case[1] :
                liste.append(case)
    return liste

def places_possibles_sur_carre(valeur,numero_de_carre) :
    liste=[]
    if valeur in valeurs_manquantes(numero_de_carre) :
        for case in cases_a_traiter :
            if numero_carre(case)==numero_de_carre and valeur in case[1] :
                liste.append(case)
    return liste



def ligne(numero) :
    liste=[TAILLE*(numero//TAILLE)+k for k in range(TAILLE)]
    if numero in liste:
        liste.remove(numero)
    return liste

def colonne(numero) :
    liste=[TAILLE*k+numero%TAILLE for k in range(TAILLE)]
    if numero in liste:
        liste.remove(numero)
    return liste

def carre(numero) :
    abscisse=((numero%TAILLE)//COTE)*COTE
    ordonnee=(numero//(TAILLE*COTE))*COTE*TAILLE
    liste=[]
    for i in range(COTE):
        liste=liste+[TAILLE*i+ordonnee+abscisse+k for k in range(COTE)]
    if numero in liste :
        liste.remove(numero)
    return liste

def unique_sur_une_ligne(valeur,numero_de_ligne) :
    return len(places_possibles_sur_ligne(valeur,numero_de_ligne))==1

def unique_sur_une_colonne(valeur,numero_de_colonne) :
    return len(places_possibles_sur_colonne(valeur,numero_de_colonne))==1

def unique_dans_un_carre(valeur,numero_de_carre) :
    return len(places_possibles_sur_carre(valeur,numero_de_carre))==1

def traitement_par_places(case) :
    if len(case[1])>1 :
        for valeur in range(TAILLE):
            if valeur in case[1] :
                if unique_sur_une_ligne(valeur,numero_ligne(case)) :
                    case[1]=[valeur]
                if unique_sur_une_colonne(valeur,numero_colonne(case)) :
                    case[1]=[valeur]
                if unique_dans_un_carre(valeur,numero_carre(case)) :
                    case[1] = [valeur]

def traitement(case) :
    traitement_par_places(case)
    if len(case[1])==1 :
        for case_en_cours in cases_a_traiter :
            if case_en_cours[0] in ligne(case[0])+colonne(case[0])+carre(case[0]) and case[1][0] in case_en_cours[1]:
                case_en_cours[1].remove(case[1][0])
            if len(case_en_cours[1])==0:
                return False
        cases_traitees.append(case)
        cases_a_traiter.remove(case)
    return True

def traitement_force(cases_traitees,cases_a_traiter) :
    taille_mini=1
    while taille_mini ==1 and len(cases_a_traiter)>0 :
        taille_mini=len(cases_a_traiter[0][1])
        for case in cases_a_traiter :
            if len(case[1])<taille_mini :
                taille_mini=len(case[1])
            if not(traitement(case)):
                return False
    return True


def reculerchoix():
    global cases_a_traiter,cases_traitees,enregistrement

    if len(enregistrement)>0:
        cases_traitees=deepcopy(enregistrement[-1][0])
        cases_a_traiter=deepcopy(enregistrement[-1][1])
        del enregistrement[-1]
        return True
    else:
       return False



def fairechoix():
    global cases_a_traiter,cases_traitees,enregistrement

    min = len(cases_a_traiter[0][1])
    indice_min = 0
    for indice in range(len(cases_a_traiter)) :
        case = cases_a_traiter[indice]
        if len(case[1])<min and len(case[1])>0:
            min = len(case[1])
            indice_min=indice
    while len(cases_a_traiter)>0 and len(cases_a_traiter[indice_min][1])==0 :
        reculerchoix()
    if len(cases_a_traiter)==0 :
        stock()
        del enregistrement[-1]
    else :
        temp=cases_a_traiter[indice_min][1][0]# choix du premier element de la liste
        del cases_a_traiter[indice_min][1][0]
        enregistrement.append([deepcopy(cases_traitees),deepcopy(cases_a_traiter)])
        cases_a_traiter[indice_min][1]=[temp]

def dessin() :
    global cases_traitees,solution
    tableau = [[0]*TAILLE for k in range (TAILLE)]
    for case in cases_traitees :
        ligne=case[0]//TAILLE
        colonne=case[0]%TAILLE
        tableau[ligne][colonne]=case[1][0]
    for k in range(TAILLE) :
        print(tableau[k])

def stock():
    global cases_traitees,solution

    tableau = [[0]*TAILLE for k in range (TAILLE)]
    for case in cases_traitees :
        ligne=case[0]//TAILLE
        colonne=case[0]%TAILLE
        tableau[ligne][colonne]=case[1][0]
    solution.append(tableau)

def sup_doublon(maliste):
    purge=[]
    for elt in maliste:
        if elt not in(purge):
            purge.append(elt)
    return purge

def donnee(liste_donnees) :
    print("Les données :")
    tableau = [[0]*TAILLE for k in range (TAILLE)]
    for case in liste_donnees :
        ligne=case[0]//TAILLE
        colonne=case[0]%TAILLE
        tableau[ligne][colonne]=case[1]
    for k in range(TAILLE) :
        print(tableau[k])


def conversion_donnees_en_tableau(liste_donnees):
    tableau=[0]*LONGUEUR
    for case in liste_donnees :
        tableau[case[0]]=case[1]
    return tableau

def conversion_solution_en_tableau(solution):
    tableau=[]
    for ligne in solution :
        for element in ligne :
            tableau.append(element)
    return tableau

def resol():
    global cases_traitees,cases_a_traiter,enregistrement,nbretour

    etatsol=True
    nbretour=0
    while len(cases_a_traiter)>0:
        etat=traitement_force(cases_traitees,cases_a_traiter)
        if not(etat):
            etatsol=reculerchoix()
        elif len(cases_a_traiter)>0:
            fairechoix()
            nbretour+=1
        if not(etatsol):
            if len(cases_a_traiter) == 0:
                dessin()
            break
    if etatsol:
        stock()


def generation(n):
    global cases_traitees,cases_a_traiter,enregistrement,nbretour,liste_donnees
    listecases=[]
    liste_donnees=[]
    cases=[k for k in range(0,LONGUEUR)]
    listecases=sample(cases,n)
    for case in listecases:
        valeurs_possibles=[k for k in range(1,TAILLE+1)]
        for case_en_cours in liste_donnees :
            if case_en_cours[0] in ligne(case)+colonne(case)+carre(case) :
                if case_en_cours[1] in valeurs_possibles:
                    valeurs_possibles.remove(case_en_cours[1])
        if len(valeurs_possibles)!=0:
            liste_donnees.append([case,choice(valeurs_possibles)])
        else:
            return False
    return liste_donnees

def generation_probleme() :
    global cases_traitees,cases_a_traiter,enregistrement,nbretour,solution,liste_donnees
    solution=[]
    nb_cases=randint((TAILLE-1)*COTE,(TAILLE+1)*COTE)
    liste_donnees=[]
    enregistrement=[]
    while len(solution)>1 or len(solution)==0 :
        cases_a_traiter=[[k,[i for i in range(1,TAILLE+1)]] for k in range(TAILLE*TAILLE)]
        cases_traitees=[]
        enregistrement=[]
        solution=[]
        nbsol=0
        liste_donnees=generation(nb_cases)
        if liste_donnees:
            initialisation(liste_donnees)
            resol()
            while len(enregistrement)>1:
                if nbretour==0:
                    del enregistrement[-1]
                cases_traitees=enregistrement[-1][0]
                cases_a_traiter= enregistrement[-1][1]
                resol()
                solution=sup_doublon(solution)
                if len(solution)>1 :
                    generation_probleme()

def ecriture_solution():
    global enonces_difficiles,enonces_faciles,enonces_moyens,solutions_difficiles,solutions_faciles,solutions_moyens
    enonce=conversion_donnees_en_tableau(liste_donnees)
    resultat=conversion_solution_en_tableau(solution[0])
    if nbretour == 0 :
        enonces_faciles.append(enonce)
        solutions_faciles.append(resultat)
    elif nbretour ==1 :
        enonces_moyens.append(enonce)
        solutions_moyens.append(resultat)
    else :
        enonces_difficiles.append(enonce)
        solutions_difficiles.append(resultat)
    print(len(enonces_difficiles),len(enonces_faciles),len(enonces_moyens))

def plusieurs_problemes(faciles,moyens,difficiles) :
    global enonces_difficiles, enonces_faciles, enonces_moyens
    global solutions_difficiles,solutions_faciles,solutions_moyens
    enonces_difficiles=[]
    enonces_faciles=[]
    enonces_moyens=[]
    solutions_difficiles=[]
    solutions_faciles=[]
    solutions_moyens=[]
    while len(enonces_faciles)<faciles or len(enonces_moyens)<moyens or len(enonces_difficiles)<difficiles :
        generation_probleme()
        ecriture_solution()
    '''while len(enonces_faciles)>faciles :
        enonces_faciles.remove(enonces_faciles[-1])
        solutions_faciles.remove(solutions_faciles[-1])
    while len(enonces_moyens)>moyens :
        enonces_moyens.remove(enonces_moyens[-1])
        solutions_faciles.remove(solutions_faciles[-1])
    while len(enonces_difficiles)>difficiles :
        enonces_difficiles.remove(enonces_difficiles[-1])
        solutions_difficiles.remove(solutions_difficiles[-1])'''


def conversion_liste_en_texte(tableau,fichier) :
    f=open(fichier,'a')
    f.write("[")
    for i in range(TAILLE) :
        f.write("['")
        for j in range(TAILLE) :
            if tableau[TAILLE*i+j]!=0:
                f.write(str(tableau[TAILLE*i+j]))
            if j != TAILLE-1 :
                f.write("','")
            else :
                f.write("']")
        if i != TAILLE-1 :
            f.write(",\n")
    f.write("]")
    f.close()

def ecriture_fichier(fichier,liste_enonces,liste_solutions) :
    f = open(fichier,'w')
    n = len(liste_enonces)
    f.write("enonce =[")
    f.close()
    for i in range(n):
        conversion_liste_en_texte(liste_enonces[i],fichier)
        if i != n-1:
            f=open(fichier,'a')
            f.write(",")
        f=open(fichier,'a')
        f.write("\n")
        f.close()
    f=open(fichier,'a')
    f.write("]\n")
    f.write("solution = [")
    f.close()
    for i in range(n) :
        conversion_liste_en_texte(liste_solutions[i], fichier)
        if i != n-1:
            f=open(fichier,'a')
            f.write(",")
        f=open(fichier,'a')
        f.write("\n")
        f.close()
    f=open(fichier,'a')
    f.write("]")
    f.close()


def toutes_solutions() :
    global solution,enregistrement,cases_a_traiter,cases_traitees
    initialisation(liste_donnees)
    resol()
    while len(enregistrement)>1:
        if nbretour==0:
            del enregistrement[-1]
        cases_traitees=enregistrement[-1][0]
        cases_a_traiter= enregistrement[-1][1]
        resol()
    solution=sup_doublon(solution)



def une_solution() :
    initialisation(liste_donnees)
    resol()
    dessin()



def changer_taille(k) :
    global COTE,TAILLE,LONGUEUR,compteur_affichage,cases_a_traiter
    global cases_traitees,nbretour,enregistrement,solution,affichage
    COTE=k
    TAILLE=COTE*COTE
    LONGUEUR=TAILLE*TAILLE
    compteur_affichage = 0
    cases_traitees=[]
    cases_a_traiter=[[k,[i for i in range(1,TAILLE+1)]] for k in range(TAILLE*TAILLE)]
    enregistrement=[]
    solution=[]
    nbretour=0
    nbsol=0
    choisir_action()

def afficher_solution(tableau) :
    global tableau_solution
    fenetre_solution=Frame(sudo, width=TAILLE*7+10, height=TAILLE*5+10,bd=2)
    fenetre_solution.grid(row=3,column = TAILLE+3)
    tableau_solution=[[]for k in range(len(tableau))]
    for i in range(len(tableau)) :
        for j in range(len(tableau[i])) :
            tableau_solution[i].append(Label(fenetre_solution,text=str(tableau[i][j])))
            tableau_solution[i][j].grid(in_=fenetre_solution,row=7+TAILLE+i,column=j)


def lancer_resolution():
    global liste_donnees,cases_a_traiter
    bouton_lancer_resolution.grid_forget()
    liste_donnees=[]
    for i in range(TAILLE):
        for j in range(TAILLE):
            if liste_entrees[i][j].get() != '':
                liste_donnees.append([TAILLE*i+j,int(liste_entrees[i][j].get())])
    initialisation(liste_donnees)
    une_solution()
    if len(solution)==1:
        afficher_solution(solution[0])
    else :
        echec.grid(row=TAILLE+5,column=5)



def afficher_toutes_sols():
    global compteur_affichage,tableau_solution
    if compteur_affichage<len(solution) :
        for i in range(len(tableau_solution)):
            for j in range(len(tableau_solution[i])) :
                tableau_solution[i][j].grid_forget()
        afficher_solution(solution[compteur_affichage])
        compteur_affichage = compteur_affichage+1
    else :
        succes.grid_forget()
        bouton_afficher_nouvelle_sol.grid_forget()
        termine.grid(row=5, column= 5)

def toutes_sol():
    global liste_donnees,cases_a_traiter,compteur_affichage
    bouton_toutes_solutions.grid_forget()
    liste_donnees=[]
    compteur_affichage=0
    for i in range(TAILLE):
        for j in range(TAILLE):
            if liste_entrees[i][j].get() != '':
                liste_donnees.append([TAILLE*i+j,int(liste_entrees[i][j].get())])
    initialisation(liste_donnees)
    toutes_solutions()
    if len(solution)>0:
        succes.configure(text='Il y a '+str(len(solution))+' solution(s)')
        succes.grid(row=TAILLE+5,column=5)
        bouton_afficher_nouvelle_sol.grid(row=TAILLE+6,column=5)
    else :
        echec.grid(row=TAILLE+5,column=5)


def resoudre():
    effacer_resolution()
    creer_grille()
    bouton_lancer_resolution.grid(row=5+TAILLE,column=TAILLE//2)

def ttes_sol():
    effacer_resolution()
    creer_grille()
    bouton_toutes_solutions.grid(row=5+TAILLE,column=TAILLE//2)

def generer():
    effacer_resolution()
    text_nb_faciles.grid(column=2,row=0)
    nb_faciles.grid(column=3,row=0)
    text_nb_moyens.grid(column=2,row = 1)
    nb_moyens.grid(column=3,row=1)
    text_nb_difficiles.grid(column=2,row=2)
    nb_difficiles.grid(column=3,row=2)
    bouton_recherche.grid(column=3, row=3)

def lancer_recherche() :
    global nombre_faciles, nombre_moyens,nombre_difficiles
    global nb_faciles,nb_difficiles,nb_moyens
    effacer_resolution()
    if nb_faciles.get().isnumeric() :
        nombre_faciles=int(nb_faciles.get())
    else :
        nombre_faciles=0
    if nb_moyens.get().isnumeric() :
        nombre_moyens=int(nb_moyens.get())
    else :
        nombre_moyens=0
    if nb_difficiles.get().isnumeric() :
        nombre_difficiles=int(nb_difficiles.get())
    else :
        nombre_difficiles=0
    plusieurs_problemes(nombre_faciles,nombre_moyens,nombre_difficiles)
    bouton_faciles.grid(row = 0, column = 2)
    bouton_moyens.grid(row = 0,column=3)
    bouton_difficiles.grid(row=0,column=4)


def afficher_faciles() :
    global nombre_faciles
    if nombre_faciles >0 :
        nombre_faciles -=1
        afficher_probleme(enonces_faciles[nombre_faciles],solutions_faciles[nombre_faciles])

def afficher_moyens() :
    global nombre_moyens
    if nombre_moyens > 0 :
        nombre_moyens -=1
        afficher_probleme(enonces_moyens[nombre_moyens],solutions_moyens[nombre_moyens])


def afficher_difficiles():
    global nombre_difficiles
    if nombre_difficiles >0 :
        nombre_difficiles -= 1
        afficher_probleme(enonces_difficiles[nombre_difficiles],solutions_difficiles[nombre_difficiles])




def afficher_probleme(enonce,solution):
    global affichage
    for element in affichage :
            element.grid_forget()
    affichage = []
    espace.grid_forget()
    for n in range(LONGUEUR) :
        if enonce[n] != 0 :
            affichage.append(Label(sudo, text=str(enonce[n])))
        else :
            affichage.append(Label(sudo, text=' '))
        affichage[n].grid(row = n//TAILLE+3, column = n % TAILLE +6)
    espace.grid(row=0,column=TAILLE+7)
    for n in range(LONGUEUR) :
        affichage.append(Label(sudo, text = str(solution[n])))
        affichage[LONGUEUR+n].grid(row = n//TAILLE + 3, column = n%TAILLE + TAILLE +8)



def choisir_action() :
    effacer_resolution()
    tableau_solution=[[]for k in range(TAILLE)]
    bouton_resoudre.grid(row=0,column=1)
    bouton_toutes_sol.grid(row=1,column=1)
    bouton_generer.grid(row=2,column=1)

def creer_grille() :
    global liste_entrees
    effacer_resolution()
    n=len(liste_entrees)
    grille = Frame(sudo, width=TAILLE*7+10, height=TAILLE*5+10,bd=2)
    grille.grid(row=3,column = 2)
    liste_entrees=[[]for k in range(TAILLE)]
    for i in range(TAILLE):
        for j in range(TAILLE) :
            liste_entrees[i].append(Entry(grille,width=5))
            liste_entrees[i][j].grid(in_=grille,row=3+i,column=3+j)

boutons_taille=[]
boutons_taille.append(Button(sudo, text='4x4',command=lambda:changer_taille(2)))
boutons_taille.append(Button(sudo, text='9x9',command=lambda:changer_taille(3)))
boutons_taille.append(Button(sudo, text='16x16',command=lambda:changer_taille(4)))

for k in range(3) :
    boutons_taille[k].grid(row=k,column=0)


bouton_resoudre=Button(sudo, text='resoudre',command=resoudre)
bouton_toutes_sol=Button(sudo, text='Toutes solutions',command=ttes_sol)
bouton_generer=Button(sudo,text='generer problemes',command=generer)


bouton_lancer_resolution=Button(sudo, text='lancer résolution',command=lancer_resolution)
bouton_toutes_solutions=Button(sudo, text='lancer résolution',command=toutes_sol)

bouton_afficher_nouvelle_sol=Button(sudo,command=afficher_toutes_sols,text='afficher nouvelle solution' )
bouton_recherche=Button(sudo,command=lancer_recherche,text='Lancer la recherche')

bouton_faciles=Button(sudo, text='problèmes faciles', command = afficher_faciles)
bouton_moyens=Button(sudo, text='problèmes moyens', command = afficher_moyens)
bouton_difficiles=Button(sudo, text='problèmes difficiles', command = afficher_difficiles)

def effacer_resolution() :
    global affichage, liste_entrees,tableau_solution,cases_a_traiter,cases_traitees
    global enregistrement,solution, nbretour,nbsol
    cases_a_traiter=[[k,[i for i in range(1,TAILLE+1)]] for k in range(TAILLE*TAILLE)]
    cases_traitees=[]
    enregistrement=[]
    solution=[]
    nbretour=0
    nbsol=0
    bouton_recherche.grid_forget()
    text_nb_difficiles.grid_forget()
    text_nb_faciles.grid_forget()
    text_nb_moyens.grid_forget()
    nb_faciles.grid_forget()
    nb_moyens.grid_forget()
    nb_difficiles.grid_forget()
    bouton_recherche.grid_forget()
    bouton_difficiles.grid_forget()
    bouton_faciles.grid_forget()
    bouton_moyens.grid_forget()
    bouton_lancer_resolution.grid_forget()
    bouton_toutes_solutions.grid_forget()
    bouton_afficher_nouvelle_sol.grid_forget()
    succes.grid_forget()
    echec.grid_forget()
    termine.grid_forget()
    espace.grid_forget()
    for element in affichage :
        element.grid_forget()
    affichage = []
    for liste in liste_entrees :
        for element in liste :
            element.grid_forget()
    liste_entrees=[[]for k in range(TAILLE)]
    for liste in tableau_solution :
        for element in liste :
            element.grid_forget()




sudo.mainloop()

