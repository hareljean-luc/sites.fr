﻿#-*-coding: utf-8-*

from copy import deepcopy
from random import *

COTE=3
TAILLE=COTE*COTE
nbretour=0


def initialisation(liste_donnees):
    for case in liste_donnees :
        numero=case[0]
        cases_a_traiter[numero][0]=numero
        cases_a_traiter[numero][1]=[case[1]]


def ligne(numero) :
    liste=[TAILLE*(numero//TAILLE)+k for k in range(TAILLE)]
    liste.remove(numero)
    return liste

def colonne(numero) :
    liste=[TAILLE*k+numero%TAILLE for k in range(TAILLE)]
    liste.remove(numero)
    return liste

def carre(numero) :
    abscisse=((numero%TAILLE)//COTE)*COTE
    ordonnee=(numero//(TAILLE*COTE))*COTE*TAILLE
    liste=[]
    for i in range(COTE):
        liste=liste+[9*i+ordonnee+abscisse+k for k in range(COTE)]
    liste.remove(numero)
    return liste

def traitement(case) :

    if len(case[1])==1 :
        for case_en_cours in cases_a_traiter :
            if case_en_cours[0] in ligne(case[0])+colonne(case[0])+carre(case[0]) and case[1][0] in case_en_cours[1]:
                case_en_cours[1].remove(case[1][0])

            if len(case_en_cours[1])==0:
                return False

        cases_traitees.append(case)
        cases_a_traiter.remove(case)

    return True

def traitement_force(cases_traitees,cases_a_traiter) :
    taille_mini=1
    while taille_mini ==1 and len(cases_a_traiter)>0 :

        taille_mini=len(cases_a_traiter[0][1])

        for case in cases_a_traiter :

            if len(case[1])<taille_mini :
                taille_mini=len(case[1])
            if not(traitement(case)):
                return False

    return True


def reculerchoix():
    global cases_a_traiter,cases_traitees,enregistrement

    if len(enregistrement)>0:
        cases_traitees=deepcopy(enregistrement[-1][0])
        cases_a_traiter=deepcopy(enregistrement[-1][1])
        del enregistrement[-1]
        return True
    else:
       return False



def fairechoix():

    global cases_a_traiter,cases_traitees,enregistrement

    min = len(cases_a_traiter[0][1])
    indice_min = 0
    for indice in range(len(cases_a_traiter)) :
        case = cases_a_traiter[indice]
        if len(case[1])<min and len(case[1])>0:
            min = len(case[1])
            indice_min=indice

    temp=cases_a_traiter[indice_min][1][0]# choix du premier element de la liste
    del cases_a_traiter[indice_min][1][0]
    enregistrement.append([deepcopy(cases_traitees),deepcopy(cases_a_traiter)])
    cases_a_traiter[indice_min][1]=[temp]

def dessin() :
    global cases_traitees,solution
    tableau = [[0]*TAILLE for k in range (TAILLE)]
    for case in cases_traitees :
        ligne=case[0]//TAILLE
        colonne=case[0]%TAILLE
        tableau[ligne][colonne]=case[1][0]
    for k in range(TAILLE) :
        print(tableau[k])

def stock():
    global cases_traitees,solution

    tableau = [[0]*TAILLE for k in range (TAILLE)]
    for case in cases_traitees :
        ligne=case[0]//TAILLE
        colonne=case[0]%TAILLE
        tableau[ligne][colonne]=case[1][0]
    solution.append(tableau)

def sup_doublon(maliste):

    purge=[]
    for elt in maliste:
        if elt not in(purge):
            purge.append(elt)
    return purge

def donnee(liste_donnees) :
    print("Les données :")
    tableau = [[0]*TAILLE for k in range (TAILLE)]
    for case in liste_donnees :
        ligne=case[0]//TAILLE
        colonne=case[0]%TAILLE
        tableau[ligne][colonne]=case[1]
    for k in range(TAILLE) :
        print(tableau[k])

def ecrire_donnees():
    tableau=[0]*81
    for case in liste_donnees :
        tableau[case[0]]=case[1]
    f=open('donnees.js','w')
    f.write('[')
    for i in range(8) :
        f.write('\n')
        f.write("['")
        for j in range(8):
            if tableau[9*i+j]!=0 :
                f.write(str(tableau[9*i+j]))
            f.write("','")
        if tableau[9*i+8]!=0 :
            f.write(str(tableau[9*i+8]))
        f.write("'],")
    f.write("['")
    for j in range(8):
        if tableau[9*i+j]!=0 :
            f.write(str(tableau[9*i+j]))
        f.write("','")
    if tableau[9*i+8]!=0 :
        f.write(str(tableau[9*i+8]))
    f.write("']]")
    f.close()



def resol():
    global cases_traitees,cases_a_traiter,enregistrement,nbretour

    etatsol=True
    nbretour=0


    while len(cases_a_traiter)>0:

        etat=traitement_force(cases_traitees,cases_a_traiter)

        if not(etat):

            etatsol=reculerchoix()
            nbretour+=1
        elif len(cases_a_traiter)>0:

            fairechoix()

        if not(etatsol):

            break


    if etatsol:

        stock()

def generation(n):

    listecases=[]
    liste_donnees=[]
    cases=[k for k in range(0,81)]
    listecases=sample(cases,n)


    for case in listecases:
        valeurs_possibles=[k for k in range(1,10)]
        for case_en_cours in liste_donnees :
            if case_en_cours[0] in ligne(case)+colonne(case)+carre(case) :
                if case_en_cours[1] in valeurs_possibles:
                    valeurs_possibles.remove(case_en_cours[1])

        if len(valeurs_possibles)!=0:
            liste_donnees.append([case,choice(valeurs_possibles)])
        else:
            return False


    return liste_donnees


solution=[]
nb_cases=int(input("entrez le nombre de cases connues de la grille"))
def generation_solution():
    global cases_traitees,cases_a_traiter,enregistrement,nbretour,solution
    while len(solution)>1 or len(solution)==0 :
        cases_a_traiter=[[k,[i for i in range(1,TAILLE+1)]] for k in range(TAILLE*TAILLE)]

        cases_traitees=[]

        enregistrement=[]
        solution=[]

        nbsol=0
        liste_donnees=generation(nb_cases)
        if liste_donnees:
            donnee(liste_donnees)
            initialisation(liste_donnees)
            resol()


            while len(enregistrement)>1:

                if nbretour==0:
                    del enregistrement[-1]

                cases_traitees=enregistrement[-1][0]
                cases_a_traiter= enregistrement[-1][1]

                resol()

            solution=sup_doublon(solution)

            print("il y a ",len(solution),"solution(s) avec ",nbretour,"retours choix")

generation_solution()
for sol in solution:
    print("Solution")
    for i in range(TAILLE):
        print(sol[i])




