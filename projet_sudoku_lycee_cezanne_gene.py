﻿#-*-coding: utf-8-*

from copy import deepcopy
from random import *
import csv

COTE=3
TAILLE=COTE*COTE


def initialisation(liste_donnees):
    for case in liste_donnees :
        numero=case[0]
        cases_a_traiter[numero][0]=numero
        cases_a_traiter[numero][1]=[case[1]]


def ligne(numero) :
    liste=[TAILLE*(numero//TAILLE)+k for k in range(TAILLE)]
    liste.remove(numero)
    return liste

def colonne(numero) :
    liste=[TAILLE*k+numero%TAILLE for k in range(TAILLE)]
    liste.remove(numero)
    return liste

def carre(numero) :
    abscisse=((numero%TAILLE)//COTE)*COTE
    ordonnee=(numero//(TAILLE*COTE))*COTE*TAILLE
    liste=[]
    for i in range(COTE):
        liste=liste+[9*i+ordonnee+abscisse+k for k in range(COTE)]
    liste.remove(numero)
    return liste

def traitement(case) :
    global Mini
    if len(case[1])==1 :
        for case_en_cours in cases_a_traiter :
            if case_en_cours[0] in ligne(case[0])+colonne(case[0])+carre(case[0]) and case[1][0] in case_en_cours[1]:
                case_en_cours[1].remove(case[1][0])
            if len(case_en_cours[1])==0:
                Mini=0
        cases_traitees.append(case)
        cases_a_traiter.remove(case)


def traitement_force(cases_traitees,cases_a_traiter) :
    taille_mini=1
    while taille_mini ==1 and len(cases_a_traiter)>0 :

        taille_mini=len(cases_a_traiter[0][1])
        for case in cases_a_traiter :

            if len(case[1])<taille_mini :
                taille_mini=len(case[1])
            traitement(case)




def reculerchoix():
    global cases_a_traiter,cases_traitees,enregistrement

    if len(enregistrement)>2:
        cases_traitees=deepcopy(enregistrement[-1][0])
        cases_a_traiter=deepcopy(enregistrement[-1][1])
        del enregistrement[-1]
        return True
    else:
       return False



def fairechoix():
    global cases_a_traiter,cases_traitees,enregistrement

    temp=cases_a_traiter[0][1][0]
    del cases_a_traiter[0][1][0]

    enregistrement.append([deepcopy(cases_traitees),deepcopy(cases_a_traiter)])

    del cases_a_traiter[0][1][1:]
    cases_a_traiter[0][1][0]=temp # choix du premier element de la liste



def dessin() :
    tableau = [[0]*TAILLE for k in range (TAILLE)]
    for case in cases_traitees :
        ligne=case[0]//TAILLE
        colonne=case[0]%TAILLE
        tableau[ligne][colonne]=case[1][0]
    for k in range(TAILLE) :
        print(tableau[k])

def fichiercsv():

    tableau = [[0]*TAILLE for k in range (TAILLE)]
    for case in cases_traitees :
        ligne=case[0]//TAILLE
        colonne=case[0]%TAILLE
        tableau[ligne][colonne]=case[1][0]

    f=open('sudoku.csv','w')
    for lignetab in tableau:

        lignecsv=csv.writer(f)
        lignecsv.writerow(lignetab)

    f.close

def donnee(liste_donnees) :
    print("Les données :")
    tableau = [[0]*TAILLE for k in range (TAILLE)]
    for case in liste_donnees :
        ligne=case[0]//TAILLE
        colonne=case[0]%TAILLE
        tableau[ligne][colonne]=case[1]
    for k in range(TAILLE) :
        print(tableau[k])

def generation(n):
    listecases=[]
    liste_donnees=[]
    cases=[k for k in range(0,81)]
    for i in range(n):
        listecases.append(choice(cases))
        cases.remove(listecases[-1])

    for case in listecases:
        valeurs_possibles=[k for k in range(1,10)]
        for case_en_cours in liste_donnees :
            if case_en_cours[0] in ligne(case)+colonne(case)+carre(case) :
                if case_en_cours[1] in valeurs_possibles:
                    valeurs_possibles.remove(case_en_cours[1])


        liste_donnees.append([case,choice(valeurs_possibles)])


    return liste_donnees


def resol(liste_donnees):


    global Mini,cases_a_traiter,cases_traitees
    initialisation(liste_donnees)

    etat=True
    nbretour=0

    while len(cases_a_traiter)>0:

        traitement_force(cases_traitees,cases_a_traiter)

        if len(cases_a_traiter)>0 and Mini==0:
            Mini=1
            etat=reculerchoix()
            nbretour+=1

        if etat:
            traitement_force(cases_traitees,cases_a_traiter)
        else:
            print("Pas de Solution")
            break

        if len(cases_a_traiter)>0 and Mini==1:
            fairechoix()

    if etat:
        print("Solution avec",nbretour,"retourchoix")


    return nbretour

##Solution directe sans retour
##liste_donnees=[[0,4],[4,2],[11,2],[13,8],[14,5],[15,4],[16,3]]
##liste_donnees+=[[18,8],[19,1],[22,9],[23,6],[27,3],[32,2],[34,8],[35,1]]
##liste_donnees+=[[36,2],[37,7],[43,4],[44,3],[45,6],[46,5],[48,8],[53,2]]
##liste_donnees+=[[54,1],[57,2],[58,4],[61,7],[62,5],[64,8],[65,4]]
##liste_donnees+=[[66,3],[67,1],[69,9],[76,5],[80,4]]

##Solution avec 3 retours
##liste_donnees=[[0,5],[2,8],[7,3],[10,7],[13,8],[15,5],[20,3],[21,4]]
##liste_donnees+=[[22,5],[23,6],[26,9],[27,2],[30,9],[31,7],[37,8],[43,5]]
##liste_donnees+=[[49,6],[50,4],[53,1],[54,6],[57,3],[58,1],[59,9],[60,8]]
##liste_donnees+=[[65,7],[67,4],[70,9],[73,9],[78,6],[80,3]]

##Expert 77 retours
##liste_donnees=[[5,4],[6,6],[7,8],[9,5],[10,9],[16,3],[18,7],[26,9],[27,4]]
##liste_donnees+=[[28,2],[30,3],[37,1],[41,6],[47,3],[48,4],[50,2],[51,1]]
##liste_donnees+=[[54,8],[55,7],[56,1],[62,6],[66,6],[78,4]]

##Expert+907 retours
##liste_donnees=[[4,8],[6,4],[8,9],[9,9],[16,6],[17,2],[18,6],[28,7],[34,1]]
##liste_donnees+=[[35,3],[36,1],[37,5],[40,4],[46,3],[49,9],[56,8],[61,4]]
##liste_donnees+=[[62,5],[68,1],[69,7],[74,2],[77,3]]

nbretour=10
while nbretour>3:

    liste_donnees=generation(17)
    donnee(liste_donnees)
    Mini=1
    cases_a_traiter=[[k,[i for i in range(1,TAILLE+1)]] for k in range(TAILLE*TAILLE)]

    cases_traitees=[]

    enregistrement=[deepcopy(cases_traitees),deepcopy(cases_a_traiter)]
    nbretour=resol(liste_donnees)


dessin()

fichiercsv()





